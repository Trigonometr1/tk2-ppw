$(function() {
    $thumbnails = $('div.container').children().last().children()
    $imgs = $('.container img')
    $form_pp = $('#form-pengumuman')
    
    var prov_user = $('#username').data("user")
    var kode_prov

    $('#form-pengumuman label').eq(1).empty().append('Prioritas: <i class="fas fa-info-circle"></i>')

    tippy('#form-pengumuman i', {
        content:"Prioritas lebih tinggi akan ditampilkan lebih awal",
    })

    tippy('.container img', {
        content:"Buat pengumuman",
    })    


    $thumbnails.each(function() {
        if ($(this).data("prov") !== prov_user) {
            $(this).hide()
        } else {
            kode_prov = $(this).data("kode-prov")
        }
    })



    $form_pp.submit(function(event){
        event.preventDefault()
        event.stopImmediatePropagation()

        var token = $("#form-pengumuman").find('input[name=csrfmiddlewaretoken]').val()

        $.ajax({
            method: "POST",
            headers: {'X-CSRFToken': token},
            url: document.location.href,
            data: $form_pp.serialize(),
            dataType: 'json',
            success: function(data) {
                $('#messages').empty()
                if (data.status === 'success') {
                    $('#messages').append('\
                    <div class="alert alert-success alert-dismissible fade show" role="alert">\
                        <h4 class="alert-heading">'+data.message+'</h4>\
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                            <span aria-hidden="true">&times;</span>\
                        </button>\
                        <p class="mb-0">Pengumuman yang dibuat</p>\
                        <hr>\
                        <p class="mb-0 detail">Judul: <span>'+data.pengumuman.judul+'</span></p>\
                        <p class="mb-0 detail">Prioritas: <span>'+data.pengumuman.prioritas+'</span></p>\
                        <p class="mb-0 detail">Isi pengumuman: <span>'+data.pengumuman.isi_pengumuman+'</span></p>\
                    </div>\
                    ')
                } else if (data.status === 'error') {
                    $('#messages').append('\
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">\
                        <h4 class="alert-heading">Gagal membuat pengumuman</h4>\
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                            <span aria-hidden="true">&times;</span>\
                        </button>\
                        <p class="mb-0">'+data.message+'</p>\
                    </div>\
                    ')
                }
                $form_pp[0].reset()
            },
            error: function(xhr, status, error) {
                alert(xhr.responseText)
                $form_pp[0].reset()
            }
        })

    })

    $isian = $form_pp.children().odd()

    $isian.mouseenter(function(){
        $(this).css("background-color", "#ECF0F1")
        $(this).focus()
    })
    $isian.mouseleave(function(){
        $(this).css("background-color", "#FFFFFF")
        $(this).blur()
    })

})