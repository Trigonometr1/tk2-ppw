from django.forms import ModelForm, TextInput, Textarea
from .models import Pengumuman

class PengumumanForm(ModelForm):
    class Meta:
        model = Pengumuman
        fields = ['judul', 'prioritas', 'isi_pengumuman']
        widgets = {
            'judul': TextInput(attrs={'placeholder': 'contoh: DKI Jakarta - PSBB Diperpanjang'}),
        }