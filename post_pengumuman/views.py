from django.contrib import messages
from django.db import IntegrityError
from django.http import JsonResponse
from django.shortcuts import render, redirect, reverse
from .models import Pengumuman, Pemerintah
from .forms import PengumumanForm
import json

# Create your views here.
def home(request):
    if not request.user.is_authenticated:
        return redirect(reverse('main:masuk'))
    uname_pemerintah = request.user.username
    return render(request, 'post_pengumuman/post_home.html', {'username': uname_pemerintah})

def form(request, string):
    if not request.user.is_authenticated:
        return redirect(reverse('main:masuk'))
    pemerintah_obj = Pemerintah.objects.get(provinsi = string)
    if request.is_ajax() and request.method == "POST":
        form = PengumumanForm(request.POST)
        if form.is_valid():
            pengumuman_obj = form.save(commit=False)
            pengumuman_obj.pemerintah = pemerintah_obj
            try:
                pengumuman_obj.save()
                data = {
                    'status':'success',
                    'message':'Sukses menambah pengumuman',
                }
                d_str = json.dumps(data) # str of data
                d = json.loads(d_str)    # dict object from string of data
                d.update({
                    'pengumuman': {
                    'judul':pengumuman_obj.judul, 
                    'prioritas':pengumuman_obj.get_prioritas_display(),
                    'isi_pengumuman':pengumuman_obj.isi_pengumuman,
                    }
                })
                # print(json.dumps(d))
                return JsonResponse(d)

            except IntegrityError as e:
                data = {
                    'status':'error',
                    'message':'Gagal: Duplikat pengumuman dengan judul dan prioritas yang sama.',
                }
                return JsonResponse(data)

    else:
        form = PengumumanForm()
    return render(request, 'post_pengumuman/post_form.html', {'provinsi':str(pemerintah_obj), 'form':form, 'kode_prov':string})