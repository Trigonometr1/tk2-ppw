from django.contrib.auth.models import User
from django.db import IntegrityError, transaction
from django.shortcuts import reverse
from django.test import TestCase, SimpleTestCase, Client
from .models import Pemerintah, Pengumuman

# Create your tests here.
class PostPengumumanTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username="jakarta_prov", password="adminJakarta")
        cls.data_login = {"username":"jakarta_prov", "password":"adminJakarta"}
        cls.pemerintah_jkt = Pemerintah.objects.create(provinsi='JKT')
        cls.pengumuman_1 = Pengumuman.objects.create(judul='Sebuah pengumuman', prioritas='00', pemerintah=cls.pemerintah_jkt, isi_pengumuman='Lorem ipsum dolor sit amet')
        return super().setUpTestData()

    def test_url_exist(self):
        response = self.client.post(reverse('main:masuk'), data = self.data_login)
        response = self.client.get('/post-pengumuman/')
        self.assertEqual(response.status_code,200)
        
        # based on existing pemerintah object
        response = self.client.get('/post-pengumuman/JKT/')
        self.assertEqual(response.status_code,200)

    def test_template_loaded(self):
        response = self.client.post(reverse('main:masuk'), data = self.data_login)
        response = self.client.get('/post-pengumuman/')
        self.assertTemplateUsed(response, 'post_pengumuman/post_home.html')
        self.assertTemplateUsed(response, 'base.html')

        # based on existing pemerintah object
        response = self.client.get('/post-pengumuman/JKT/')
        self.assertTemplateUsed(response, 'post_pengumuman/post_form.html')
        self.assertTemplateUsed(response, 'base.html')
    
    def test_model_and_str_method_pemerintah(self):
        self.assertTrue(Pemerintah.objects.count() == 1)
        self.assertTrue(Pemerintah.objects.get(provinsi='JKT') in Pemerintah.objects.all())
        self.assertEqual(str(self.pemerintah_jkt),'DKI Jakarta')

    def test_model_and_str_method_pengumuman(self):
        self.assertEqual(str(self.pengumuman_1),'Sebuah pengumum-Urgent-DKI Jakarta')

    def test_pengumuman_new_object_POST(self):
        self.client.post(reverse('main:masuk'), data = self.data_login)

        # request using AJAX set XMLHttpRequest in header
        response = self.client.post('/post-pengumuman/JKT/', {
            'judul':'Pengumuman untuk Jakarta', 
            'prioritas':'00', 
            'isi_pengumuman':'ini isinya',
            }, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEquals('success', response.json()['status'],)
        self.assertEquals('Sukses menambah pengumuman', response.json()['message'])

    def test_pengumuman_duplicate_object_POST(self):
        self.client.post(reverse('main:masuk'), data = self.data_login)

        # request using AJAX set XMLHttpRequest in header
        response = self.client.post('/post-pengumuman/JKT/', {
            'judul':'Sebuah pengumuman', 
            'prioritas':'00', 
            'isi_pengumuman':'Lorem ipsum dolor sit amet',
            }, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})

        self.assertEquals('error', response.json()['status'])
        self.assertEquals('Gagal: Duplikat pengumuman dengan judul dan prioritas yang sama.', response.json()['message'])

    def test_post_pengumuman_url_without_login_redirected(self):
        response = self.client.get(reverse('post_pengumuman:home'))
        self.assertEqual(response.status_code, 302)

        response = self.client.get(reverse('post_pengumuman:form', args=['JKT']))
        self.assertEqual(response.status_code, 302)
