from django.urls import path
from . import views

app_name = 'post_pengumuman'

urlpatterns = [
    path('', views.home, name='home'),
    path('<string>/', views.form, name='form'),
]
