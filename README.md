# Project TK 1 PPW Kelompok E09

[![pipeline status](https://gitlab.com/Trigonometr1/tk2-ppw/badges/master/pipeline.svg)](https://gitlab.com/Trigonometr1/tk2-ppw/commits/master)

[![coverage report](https://gitlab.com/Trigonometr1/tk2-ppw/badges/master/coverage.svg)](https://gitlab.com/Trigonometr1/tk2-ppw/-/commits/master)

Anggota kelompok:
1. Aflah Alifu Na Mappatajang Rahman - 1906353675
2. Muhammad Fachri Syarifuddin - 1906353605
3. Muhammad Fahreza Pratama Wijayanto - 1906351051
4. Niti Cahyaning Utami - 1906350894
5. Taufiq Hadi Pratama - 1906350856

Klik [pranala ini](http://risicovid2.herokuapp.com/) untuk menuju ke project.

Atau copy link berikut: http://risicovid2.herokuapp.com/

Awalnya kami punya gagasan untuk memberikan cara agar bisa melihat semua informasi terkait COVID-19 dalam satu aplikasi web. Namun, kami putuskan bahwa kami akan membuat aplikasi yang akan memuat konsep itu tetapi dengan beberapa simplifikasi dalam fitur yang diimplementasikan.

Daftar fitur yang akan diimplementasikan:
1. Melihat persentase kemungkinan terpapar COVID-19 dengan gejala-gejala yang diberikan (Hadi)
2. Form untuk menambahkan atau menghapus tips dan trik untuk menghindari terpapar COVID-19, kemudian ditampilkan di halaman utama (Reza)
3. Halaman form untuk mengunggah pengumuman dari pemerintah lokal, kemudian hasilnya ditampilkan di halaman utama (Niti)
4. Halaman utama, revisi styling dan tampilan tiap HTML dari models (Fachri)