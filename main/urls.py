from django.urls import path, re_path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    re_path(r'^masuk/$', views.masuk, name='masuk'),
    re_path(r'^daftar/$', views.daftar, name='daftar'),
    re_path(r'^keluar/$', views.keluar, name='keluar'),
]
