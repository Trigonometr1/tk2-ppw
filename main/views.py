from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.shortcuts import render, redirect, reverse
from django.views.generic import ListView
from post_pengumuman.models import Pengumuman
from tips.models import tips
from django.contrib import messages
from .forms import SignUpForm, LoginForm

class HomeView(ListView):
    template_name = 'main/home.html'
    context_object_name = 'pengumuman_list'

    def get_queryset(self):
        return Pengumuman.objects.order_by('prioritas')

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context.update({
            'tips_list':tips.objects.all(),
            'is_pemprov':self.request.user.groups.filter(name="pemprov").exists(),
        })
        return context

def masuk(request):
    if request.user.is_authenticated:
        return redirect(reverse("main:home"))
    if request.method == 'POST':
        form = LoginForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect(reverse('main:home'))
    else:
        form = LoginForm
    return render(request, 'login.html', {'form': form})

def daftar(request):
    if request.user.is_authenticated:
        return redirect(reverse("main:home"))
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            new_user_account = authenticate(
                username = form.cleaned_data["username"],
                password = form.cleaned_data["password1"]
            )
            # Langsung loginkan user yang sudah authenticated.
            login(request, new_user_account)
            return redirect(reverse('main:home'))
    else:
        form = SignUpForm
    return render(request, 'signup.html', {'form': form})

def keluar(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect(reverse('main:home'))