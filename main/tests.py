from django.contrib.auth.models import User
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver


"""@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    "Base class for functional test cases with selenium."

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()"""


class MainTestCase(TestCase):
    def setUp(self):
        self.c = Client
    def test_root_url_status_200(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        
    def test_template_yang_dipake(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "main/home.html")
        response = Client().get('/masuk/')
        self.assertTemplateUsed(response, "login.html")
        response = Client().get('/daftar/')
        self.assertTemplateUsed(response, "signup.html")

class ViewsTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user(username="user_lama", password="user123pass")

    def test_user_login_success_and_redirected(self):
        data_login = {'username':'user_lama', 'password':'user123pass'}
        response = self.client.post(reverse('main:masuk'), data=data_login)
        self.assertRedirects(response, reverse('main:home'))

    def test_non_registered_user_login_failed_and_message_shown(self):
        data_login = {'username':'random_user', 'password':'user404fail'}
        response = self.client.post(reverse('main:masuk'), data=data_login)
        response_dec = response.content.decode('utf-8')
        self.assertIn("errorlist", response_dec)
    
    def test_new_user_created_and_redirected(self):
        data_register = {'username':'user_baru', 'password1':'user001pass', 'password2':'user001pass'}
        response = self.client.post(reverse('main:daftar'), data=data_register)
        self.assertEqual(response.status_code, 302)

    def test_user_logout_success_and_redirected(self):
        data_login = {'username':'user_lama', 'password':'user123pass'}
        response = self.client.post(reverse('main:masuk'), data=data_login)
        response = self.client.get(reverse('main:keluar'))
        self.assertRedirects(response, reverse('main:home'))

    def test_register_and_login_when_active_session(self):
        # Buat session
        data_login = {'username':'user_lama', 'password':'user123pass'}
        response = self.client.post(reverse('main:masuk'), data=data_login)
        # Mencoba register ketika ada session aktif
        data_register = {'username':'user_lama', 'password1':'user001pass', 'password2':'user001pass'}
        response = self.client.post(reverse('main:daftar'), data=data_register)
        self.assertEqual(response.status_code, 302)
        # Mencoba login ketika ada session aktif
        data_login = {'username':'user_lama', 'password':'user123pass'}
        response = self.client.post(reverse('main:masuk'), data=data_login)
        self.assertEqual(response.status_code, 302)