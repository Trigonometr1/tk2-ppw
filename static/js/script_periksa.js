$(document).ready(function() {
    // TODO: Bagian index.html
    // JavaScript ubah warna navbar
    $("#navbar-base").hover(
        function() {
            $("#navbar-base").css({"background-color" : "#517394"})
        }, function() {
            $("#navbar-base").css({"background-color" : "#2c3e50"})
        }
    )
    // JavaScript memunculkan QOTD
    var qotdArr = [
        "Humans are creatures that fight their own weakness",
        "Life is not a problem to be solved, but a reality to be experienced",
        "Keep calm and carry on",
        "Live as if you were to die tomorrow. Learn as if you were to live forever",
        "The best way to predict your future is to create it"
    ]
    $("#qotd").hover(
        function() {
            var x = Math.floor((Math.random() * 5));
            $("#qotd").empty()
            $("#qotd").append(qotdArr[x])
        }, function() {
            $("#qotd").empty()
            $("#qotd").append("Arahkan mouse ke sini")
        }
    )
    var dataAJAX = {};
    var gejala = []
    $("#btn-submit").click(function(event) {
        // Dapatkan csrf token
        //dataAJAX['csrfmiddlewaretoken'].push($('input[name=csrfmiddlewaretoken]').val())
        dataAJAX["csrfmiddlewaretoken"] = $('input[name=csrfmiddlewaretoken]').val()
        // Dapatkan nama dan daerah
        //dataAJAX['nama'].push($('input[name=nama]').val())
        dataAJAX['nama'] = $('#id_nama').val()
        console.log($("#id_daerah"))
        $('select').children().each(function() {
            if($(this)[0].selected == true) {
                //dataAJAX['daerah'].push($(this).val())
                dataAJAX["daerah"] = $(this).val()
            }
        })
        // Dapatkan dataAJAX gejala
        $("#tes-gejala").children().each(function() {
            if($(this).children()[0].control.checked == true) {
                gejala.push($(this).children()[0].childNodes[0].defaultValue);
            }
        })
        event.preventDefault();
        $.ajax({
            type:"POST",
            url: "/periksa/hasil/",
            traditional : true,
            data: {
                csrfmiddlewaretoken : dataAJAX["csrfmiddlewaretoken"],
                nama : dataAJAX["nama"],
                daerah : dataAJAX["daerah"],
                gejala : gejala
            },
            success: function(response) {
                $("html").empty()
                $("html").append(response)
            }
        })
    })

    // TODO: Bagian show_pengunjung_periksa.html
    $("#btn-data").click(function(event) {
        $.ajax({
            type:"GET",
            url: "/periksa/data/",
            traditional : true,
            success: function(JSONObject) {
                for (i = 0; i < JSONObject.length; i++) {
                    $("#list-pengunjung-periksa").css({"list-style-type" : "none"})
                    $("#list-pengunjung-periksa").append(
                        `
                        <li>${JSONObject[i].nama}. Asal daerah ${JSONObject[i].daerah}</li>
                        `
                    )
                }
                $("#btn-data").hide()
            }
        })
    })
})