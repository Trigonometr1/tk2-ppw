from django.shortcuts import render, redirect
from .models import PengunjungPeriksa
from .forms import PengunjungPeriksaForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import PengunjungPeriksa
from django.http import JsonResponse

# Create your views here.
def index_periksa(request):
    context = {
        "pengunjung_periksa_form" : PengunjungPeriksaForm
    }
    return render(request, "periksa/index.html", context)

def hasil_periksa(request):
    context = {}
    if request.method == "POST":
        pengunjung_periksa_form = PengunjungPeriksaForm(request.POST)
        if pengunjung_periksa_form.is_valid():
            pengunjung = pengunjung_periksa_form.cleaned_data.get("nama")
            daerah = pengunjung_periksa_form.cleaned_data.get("daerah")
            # Buat perhitungan persentase kena COVID-19
            list_hasil = pengunjung_periksa_form.cleaned_data.get("gejala")
            persentase = round(len(list_hasil)/12 * 100, 3)
            if list_hasil[0] == "13": # "13" = "Tidak ada gejala yang muncul"
                persentase = 0
            elif list_hasil.count("13") > 0 and len(list_hasil) > 1:
                messages.add_message(request, messages.WARNING, "Pilihan saling berkontradiksi. Silakan ulangi pengisian form.")
                return redirect("periksa:index_periksa")
            if persentase <= 35:
                pesan = "Peluang Anda terkena COVID-19 cukup kecil.\
                    Selalu jaga kesehatan dan laksanakan protokol kesehatan\
                    yang berlaku di " + daerah + "."
                context["pesan"] = pesan
            elif persentase <= 70:
                pesan = "Peluang Anda terkena COVID-19 cukup besar.\
                    Direkomendasikan untuk segera melakukan rapid test\
                    di daerah " + daerah + " untuk mengetahui lebih lanjut."
                context["pesan"] = pesan
            else:
                pesan = "Peluang Anda terkena COVID-19 sangat besar.\
                    Segera melapor ke rumah sakit penanganan COVID-19 di daerah " + daerah + "."
                context["pesan"] = pesan

            pengunjung_object = PengunjungPeriksa(nama=pengunjung, daerah=daerah)
            pengunjung_object.save()
            context["persentase_str"] = str(persentase)
            context["pengunjung"] = pengunjung
        else:
            messages.add_message(request, messages.WARNING, "Panjang nama tidak boleh lebih dari 75 karakter atau tidak ada gejala yang dipilih. Silakan ulangi pengisian form.")
            messages.add_message(request, messages.WARNING, "Jika memang tidak punya gejala, silakan pilih opsi 'Tidak ada gejala yang saya alami'.")
            return redirect("periksa:index_periksa")
    return render(request, "periksa/hasil_periksa.html", context)

@login_required(login_url = "/masuk")
def show_pengunjung_periksa(request):
    return render(request, "periksa/show_pengunjung_periksa.html")

def data_pengunjung_periksa(request):
    data = list(PengunjungPeriksa.objects.values())
    return JsonResponse(data, safe=False)