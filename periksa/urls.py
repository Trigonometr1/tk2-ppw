from django.urls import path
from . import views

app_name = 'periksa'

urlpatterns = [
    path('', views.index_periksa, name='index_periksa'),
    path('hasil/', views.hasil_periksa, name='hasil_periksa'),
    path('show/', views.show_pengunjung_periksa, name='show_pengunjung_periksa'),
    path('data/', views.data_pengunjung_periksa, name='data_pengunjung_periksa')
]