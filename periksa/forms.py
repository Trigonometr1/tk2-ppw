from django import forms
from .models import PengunjungPeriksa

class PengunjungPeriksaForm(forms.Form):
    PILIHAN_GEJALA = (
        ("1", "Demam diatas 39 derajat Celcius"),
        ("2", "Batuk-batuk"),
        ("3", "Kesulitan bernapas atau napas pendek"),
        ("4", "Selalu kelelahan"),
        ("5", "Nyeri-nyeri pada otot atau tubuh"),
        ("6", "Sakit kepala"),
        ("7", "Kehilangan kemampuan mencium dan mengecap rasa"),
        ("8", "Sakit tenggorokan"),
        ("9", "Hidung tersumbat atau banyak cairan di hidung"),
        ("10", "Mual atau muntah"),
        ("11", "Diare"),
        ("12", "Pernah bertemu orang dengan gejala COVID-19 dalam selang dua minggu"),
        ("13", "Tidak ada gejala yang saya alami")
    )

    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class" : "form-control prompt",
                "placeholder" : "Masukkan Nama"
            }
        ), 
        label=""
    )

    gejala = forms.MultipleChoiceField(
        choices=PILIHAN_GEJALA,
        widget=forms.CheckboxSelectMultiple(
            attrs={"class" : "prompt"}
        ), label=""
    )

    PILIHAN_DAERAH = [
        ("Banten", "Banten"),
        ("DKI Jakarta", "DKI Jakarta"),
        ("Jawa Barat", "Jawa Barat"),
        ("Jawa Tengah", "Jawa Tengah"),
        ("DI Yogyakarta", "DI Yogyakarta"),
        ("Jawa Timur", "Jawa Timur"),
    ]

    daerah = forms.ChoiceField(
        choices=PILIHAN_DAERAH,
        widget=forms.Select(
            attrs={"class" : "prompt custom-select"}
        ), label=""
    )