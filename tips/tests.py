from django.test import TestCase, Client
from .models import tips
from django.contrib.auth.models import User
from django.urls import reverse

# Create your tests here.
class MainTestCase(TestCase):
    def setUp(self):
        self.c = Client

    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create_user(username="Zag", password="Stygius")
        cls.data_login = {"username":"Zag", "password":"Stygius"}
    
    def test_root_url_status_200(self):
        self.client.post(reverse('main:masuk'), data = self.data_login)
        response = self.client.get('/tips/')
        self.assertEqual(response.status_code, 200)
        return super().setUpClass()

        
    
    def test_template_yang_dipake(self):
        self.client.post(reverse('main:masuk'), data = self.data_login)
        response = self.client.get('/tips/')
        self.assertTemplateUsed(response, "tips.html")
        
        
    def test_buat_tips_baru(self):
        tips_test = tips.objects.create(pesan = "cuci tangan")
        count = tips.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(str(tips_test), "cuci tangan")
    
    def test_form_ada(self):
        response = Client().get("/")
        self.assertContains(response, "")

    def test_form_ada_hasilnya(self):
        self.client.post(reverse('main:masuk'), data = self.data_login)
        arg = {'pesan': 'ini_pesan'}
        self.client.post('/tips/',data = arg)
        amm_post = tips.objects.all().count()
        self.assertEqual(amm_post,1)
        response = self.client.get("/")
        self.assertContains(response,"Ini_pesan")

    def test_tips_url_tanpa_login_redirect(self):
        response = self.client.get(reverse('tips:tips'))
        self.assertEquals(response.status_code, 302)


    def test_hapus_tips_redirect(self):
        response = self.client.get(reverse('tips:hapusTips'))
        self.assertEqual(response.status_code, 302)
        
    def test_hapus_tips_success(self):
        # Login
        data_login = {'username':'user_lama', 'password':'user123pass'}
        response = self.client.post(reverse('main:masuk'), data=data_login)
        # Buat pesan
        arg = {'pesan': 'ini_pesan'}
        Client().post('/tips/',data = arg)
        # Hapus pesan
        response = self.client.get('/tips/hapus-tips/?id=1')
        self.assertEqual(response.status_code, 302)


    