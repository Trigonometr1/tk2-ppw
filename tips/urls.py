from django.urls import path

from . import views
# from main.views import home

app_name = 'tips'

urlpatterns = [
    path('', views.tips, name = 'tips'),
    path('hapus-tips/', views.hapusTips, name = 'hapusTips'),
    path('get_pembuat/', views.get_pembuat, name = "pembuat")
]