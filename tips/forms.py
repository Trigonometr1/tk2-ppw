from django import forms

class Tips(forms.Form):
    fields = ["pesan"]
    error_messages = { "required": "masih kosong"}
    format_pesan = {"placeholder":"Silakan masukkan tips", "class" : "align-items-center justify-content-center form-control"}
    pesan = forms.CharField(required = True,max_length = 500, widget =forms.Textarea(attrs=format_pesan))