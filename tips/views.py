from django.shortcuts import render, redirect
from . import forms,models
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http import JsonResponse
from django.core.serializers import serialize

# Create your views here.
def tips(request):
    if not request.user.is_authenticated:
        return redirect(reverse('main:masuk'))
    if request.method == "POST":
        formulir = forms.Tips(request.POST)
        if formulir.is_valid():
            all_tips = models.tips()
            all_tips.pesan = formulir.cleaned_data['pesan']
            all_tips.pembuat = request.user.get_username()
            all_tips.save()
        return redirect("/")
    else:
        formulir = forms.Tips()
        isi_pesan = {'tips':formulir}
    return render(request,'tips.html',isi_pesan)

def get_pembuat(request):
    id = request.GET['id']
    semua_list = models.tips.objects.filter(id__icontains = id)
    masuk_objek_list = {}

    masuk_objek_list[0] = {"nama": semua_list[0].pembuat}

    
    return JsonResponse(masuk_objek_list, safe= False)



@login_required(login_url = "/masuk")
def hapusTips(request):
    id = request.GET['id']
    item = models.tips.objects.get(id=id)
    item.delete()
    return redirect("/")
    
    